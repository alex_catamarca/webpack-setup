import React from 'react';
import ReactDOM from "react-dom";
import { decirHola } from './hola';

const App = () => {
  return (
  <div>
    {`${decirHola()}`}
  </div>
  );
};

ReactDOM.render(<App/>, document.getElementById('app-root'));